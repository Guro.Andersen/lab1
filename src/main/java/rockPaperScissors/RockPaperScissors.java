package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code new RockPaperScissors(). Then it calls the run()
    	 * method on the newly created object.
         */

        RockPaperScissors spill = new RockPaperScissors();
        spill.run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    static String humanWins = "Human wins!";
    static String computerWins = "Computer wins!";
    static String tie = "It's a tie!";
    
    public void run() {
        // Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String valg = readingUsersInput();
            int randomtall = rand.nextInt(2);
            String computerValg = rpsChoices.get(randomtall);

            String whoWins = playerWins(valg, computerValg);
            
            System.out.println("Human chose " + valg + ", computer chose " + computerValg + ". " + whoWins);
            
            if (whoWins.equals(humanWins)) {
                humanScore ++;
            }
            else if (whoWins.equals(computerWins)) {
                computerScore ++;
            }
            
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String playAgain = readInput("Do you wish to continue playing? (y/n)?");
            if (playAgain.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            

            roundCounter ++; 

        }
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String readingUsersInput() {
        String valg = readInput("Your choice (Rock/Paper/Scissors)?");
        while (true) {
            if (rpsChoices.contains(valg.toLowerCase())) {
                return valg;
            }
            else {
                valg = readInput("I do not understand " + valg + ". Could you try again?");
            }
        }
    }

    public static String playerWins(String choice, String computerChoice) {
        if (choice.equals(computerChoice)) {
            return tie;
        }
        if (choice.equals("rock")) {
            if (computerChoice.equals("scissors")) {
                return humanWins;
            }
        }
        if (choice.equals("paper")) {
            if (computerChoice.equals("rock")) {
                return humanWins;
            }
        }
        if (choice.equals("scissors")) {
            if (computerChoice.equals("paper")) {
                return humanWins;
            }
        }
        if (choice.equals("rock")) {
            if (computerChoice.equals("paper")) {
                return computerWins;
            }
        }
        if (choice.equals("paper")) {
            if (computerChoice.equals("scissors")) {
                return computerWins;
            }
        }
        else {
            if (computerChoice.equals("rock")) {
                return computerWins;
            }
        }
        return tie;
    }
}
